package redis.demo;


import org.redisson.Redisson;
import org.redisson.api.RBucket;
import org.redisson.api.RMap;
import org.redisson.api.RedissonClient;
import org.redisson.config.Config;

public class App
{
    public static void main( String[] args ) {

        System.out.println( "Hello World!" );

        Config config = new Config();

        config.useSingleServer().setAddress("redis://192.168.103.20:6379");//redis installation host and port
        config.useSingleServer().setPassword("apu123");//redis authetication just using password

        RedissonClient redisson = Redisson.create(config);

        RBucket<String> bucket = redisson.getBucket("object");
        bucket.set("This is object value apu");
        RMap<String, String> map = redisson.getMap("testmap");
        map.put("Key1", "This is Redis test map 1");

        map.put("Key2", "This is Redis test map 2");

        String objectValue = bucket.get();

        System.out.println("stored object value: " + objectValue);
        String mapValue = map.get("Key1");
        System.out.println("stored map value: " + mapValue);

        String mapValue2 = map.get("Key2");
        System.out.println("stored map value2: " + mapValue2);
        redisson.shutdown();

    }
}
